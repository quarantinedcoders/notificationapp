package com.quarantined.notification.covid19;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class Covid19Data {

	private int active;
	private int confirmed;
	private int deaths;
	private int recovered;
	private int deltaconfirmed;
	private int deltadeaths;
	private int deltarecovered;
	private String state;
	private String country;
	private String lastupdatedtime;
}
