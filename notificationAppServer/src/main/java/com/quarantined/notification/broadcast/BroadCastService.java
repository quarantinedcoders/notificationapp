package com.quarantined.notification.broadcast;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quarantined.notification.subscribe.SubscriptionService;
import com.quarantined.notification.user.User;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BroadCastService implements BroadCastServiceI {

	private final WhatsAppServiceI whatsAppService;
	private final EmailServiceI emailService;

	private static final String EMAIL_CHANNEL_NAME = "Email";
	private static final String WHATSAPP_CHANNEL_NAME = "WhatsApp";

	private final SubscriptionService subscriptionService;

	@Override
	public void sendNotificationOnWhatsApp(Long topicId, String message) {

		List<User> users = subscriptionService.getAllUsersSubscribedToGivenTopicAndChannel(topicId,
				WHATSAPP_CHANNEL_NAME);
		List<String> phoneNumbers = users.stream().map(user -> user.getPhoneno()).filter(Objects::nonNull)
				.collect(Collectors.toList());
		whatsAppService.send(phoneNumbers, message);
	}

	@Override
	public void sendNotificationOnEmail(Long topicId, String subject, String message) {

		List<User> users = subscriptionService.getAllUsersSubscribedToGivenTopicAndChannel(topicId, EMAIL_CHANNEL_NAME);
		List<String> emails = users.stream().map(user -> user.getEmail()).filter(Objects::nonNull)
				.collect(Collectors.toList());
		emailService.send(emails, subject, message);
	}
}
