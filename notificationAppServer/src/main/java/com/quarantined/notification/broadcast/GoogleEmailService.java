package com.quarantined.notification.broadcast;

import static java.lang.String.join;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quarantined.notification.config.AppProperties;
import com.quarantined.notification.config.AppProperties.GoogleMailService;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GoogleEmailService implements EmailServiceI {

	private final AppProperties appProperties;

	private static final Logger logger = LoggerFactory.getLogger(GoogleEmailService.class);

	@Override
	public void send(List<String> emails, String subject, String messageToSend) {

		logger.info("Sending emails............");

		try {
			Session session = createMailSession();

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(appProperties.getGoogleMailService().getAccount()));
			message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(join(",", emails)));
			message.setSubject(subject);
			message.setText(messageToSend);

			Transport.send(message);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Emails sent successfully............");
	}

	private Session createMailSession() {

		GoogleMailService gooleProperties = appProperties.getGoogleMailService();

		Properties properties = new Properties();
		properties.put("mail.smtp.auth", gooleProperties.getSmtpAuth());
		properties.put("mail.smtp.starttls.enable", gooleProperties.getSmtpStarttlsEnable());
		properties.put("mail.smtp.host", gooleProperties.getSmtpHost());
		properties.put("mail.smtp.port", gooleProperties.getSmtpPort());

		return Session.getInstance(properties, new Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(gooleProperties.getAccount(), gooleProperties.getPassword());
			}
		});
	}
}
