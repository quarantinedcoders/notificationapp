package com.quarantined.notification.broadcast;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

@Component
public class TwilioWhatsAppService implements WhatsAppServiceI {
	
	private static final Logger logger = LoggerFactory.getLogger(TwilioWhatsAppService.class);

	public static final String ACCOUNT_SID = "AC0cbc38ff9827db4186c394f25efee523";
	public static final String AUTH_TOKEN = "0399db606ae353105599d9d18fee14a8";

	private static final String TWILIO_WHATSAPP_INITIAL = "whatsapp:";
	private static final String TWILIO_FROM_NUMBER = "+14155238886";

	@Override
	public void send(List<String> phoneNumbers, String messageToSend) {
		
		logger.info("Sending whatsapp............");

		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		for (String phoneNumber : phoneNumbers) {
			Message.creator(new com.twilio.type.PhoneNumber(TWILIO_WHATSAPP_INITIAL + phoneNumber),
					new com.twilio.type.PhoneNumber(TWILIO_WHATSAPP_INITIAL + TWILIO_FROM_NUMBER), messageToSend)
					.create();
		}
		
		logger.info("Whatsapp sent successfully............");
	}
}
