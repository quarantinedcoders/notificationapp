package com.quarantined.notification.broadcast;

import java.util.List;

public interface EmailServiceI {
	
	void send(List<String> emails, String subject, String messageToSend);
}
