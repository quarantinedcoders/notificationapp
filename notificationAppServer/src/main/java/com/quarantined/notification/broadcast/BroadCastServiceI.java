package com.quarantined.notification.broadcast;

public interface BroadCastServiceI {
	
	void sendNotificationOnWhatsApp(Long topicId, String message);
	void sendNotificationOnEmail(Long topicId, String subject, String message);

}
