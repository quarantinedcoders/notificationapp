package com.quarantined.notification.broadcast;

import java.util.List;

public interface WhatsAppServiceI {
	
	void send(List<String> phoneNumbers, String message);
}
