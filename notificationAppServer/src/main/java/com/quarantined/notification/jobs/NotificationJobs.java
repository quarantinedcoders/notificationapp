package com.quarantined.notification.jobs;

import static com.quarantined.notification.common.StringUtils.replaceLast;
import static java.lang.String.join;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.quarantined.notification.broadcast.BroadCastServiceI;
import com.quarantined.notification.covid19.Covid19Data;
import com.quarantined.notification.covid19.Covid19ServiceI;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class NotificationJobs {

	private static final Map<String, Covid19Data> STATE_WISE_COVID19_CASES = new HashMap<>();

	private final Covid19ServiceI covid19Service;
	private final BroadCastServiceI broadCastService;

	@Scheduled(fixedDelay = 30000)
	public void checkCovid19CountInIndia() {

		System.out.println("Job started - checkCovid19CountInIndia");

		final var stateList = covid19Service.getStateWiseSummary();

		StringBuilder finalMessage = new StringBuilder();

		Covid19Data totalCases = null;
		for (Covid19Data state : stateList) {

			if (state.getState().equalsIgnoreCase("Total")) {
				totalCases = state;
				continue;
			}

			Covid19Data previousData = STATE_WISE_COVID19_CASES.getOrDefault(state.getState(), state);
			STATE_WISE_COVID19_CASES.put(state.getState(), state);

			List<String> deltaTexts = new ArrayList<>();

			createDeltaText(state.getConfirmed() - previousData.getConfirmed(), " new case", " new cases")
					.ifPresent(deltaTexts::add);
			createDeltaText(state.getRecovered() - previousData.getRecovered(), " recovery", " recoveries")
					.ifPresent(deltaTexts::add);
			createDeltaText(state.getDeaths() - previousData.getDeaths(), "death", "deaths").ifPresent(deltaTexts::add);

			String stateText = replaceLast(join(", ", deltaTexts), ", ", " and ");

			if (!Objects.toString(stateText, "").isEmpty()) {
				finalMessage.append(stateText + " in " + state.getState() +"\n");
			}
		}

		Long covid19TopicId = 1L;
		if (finalMessage.length() > 0) {
			System.out.println("There is rise in Covid19 cases, sending notification ");

			if (totalCases != null) {
				finalMessage.append("\n");
				finalMessage.append(
						"  Total cases:  (^" + totalCases.getDeltaconfirmed() + ") " + totalCases.getConfirmed() + "\n");
				finalMessage.append(
						"  Recovered  :  (^" + totalCases.getDeltarecovered() + ") " + totalCases.getRecovered() + "\n");
				finalMessage.append(
						"  Deaths     :  (^" + totalCases.getDeltadeaths() + ") " + totalCases.getDeaths() + "\n");
			}

			broadCastService.sendNotificationOnWhatsApp(covid19TopicId, finalMessage.toString());
			broadCastService.sendNotificationOnEmail(covid19TopicId, "COVID 19 Instant updates",
					finalMessage.toString());
		} else {
			System.out.println("No change in Covid19 count for India");
		}

		System.out.println("Job ended - checkCovid19CountInIndia");
	}

	private Optional<String> createDeltaText(int deltaCases, String singular, String plural) {
		String deltaText = null;
		if (deltaCases > 0) {
			deltaText = deltaCases + " " + (deltaCases == 1 ? singular : plural);
		}
		return Optional.ofNullable(deltaText);
	}
}
