package com.quarantined.notification.user;

import lombok.Data;

@Data
class UserProfile {
	private final long id;
	private final String username;
	private final String name;
	private final String email;
	private final String phoneno;
}
