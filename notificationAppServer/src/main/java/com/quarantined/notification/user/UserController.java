package com.quarantined.notification.user;

import java.net.URI;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.quarantined.notification.auth.AuthProvider;
import com.quarantined.notification.auth.SignUpRequest;
import com.quarantined.notification.common.ApiResponse;
import com.quarantined.notification.exception.AppException;
import com.quarantined.notification.exception.ResourceNotFoundException;
import com.quarantined.notification.security.CurrentUser;
import com.quarantined.notification.security.UserPrincipal;
import com.quarantined.notification.subscribe.SubscriptionRepository;
import com.quarantined.notification.topic.TopicRepository;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TopicRepository notificationTopicRepository;

	@Autowired
	private SubscriptionRepository subscriptionRepository;

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@GetMapping("/user/me")
	@PreAuthorize("hasRole('USER')")
	public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
		UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName(),
				currentUser.getEmail());
		return userSummary;
	}

	@GetMapping("/user/checkUsernameAvailability")
	public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
		Boolean isAvailable = !userRepository.existsByUsername(username);
		return new UserIdentityAvailability(isAvailable);
	}

	@GetMapping("/user/checkEmailAvailability")
	public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
		Boolean isAvailable = !userRepository.existsByEmail(email);
		return new UserIdentityAvailability(isAvailable);
	}

	/**
	 * This method should return Profile which will have name, number, email etc so
	 * change UserProfile pojo and update this rest call later
	 */
	@GetMapping("/user/{email}")
	public UserProfile getUserProfile(@PathVariable(value = "email") String email) {
		User user = userRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFoundException("User", "email", email));

		UserProfile userProfile = new UserProfile(user.getId(), user.getUsername(), user.getName(), user.getEmail(),
				user.getPhoneno());

		return userProfile;
	}

	/*
	 * Adding the details for user
	 */
	@PostMapping("/user/savedetails")
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserProfile userProfile) {

		userRepository.udpdatePhoneNo(userProfile.getPhoneno(), userProfile.getId());


		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/users/{email}")
				.buildAndExpand(userProfile.getEmail()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User details saved successfully"));
	}
	
	@GetMapping("/user/checkAdminUser/{username}")
	public Boolean isAdminUser(@PathVariable(value = "username") String username) {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
		Set<RoleName>userRoles=user.getRoles().stream().map(Role::getName).collect(Collectors.toSet());
		
		return (userRoles.contains(RoleName.ROLE_ADMIN));
	}
}
