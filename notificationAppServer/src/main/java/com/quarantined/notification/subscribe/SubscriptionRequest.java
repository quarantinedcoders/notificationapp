package com.quarantined.notification.subscribe;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SubscriptionRequest {

	@NotNull
	private List<Long> channelId;
}
