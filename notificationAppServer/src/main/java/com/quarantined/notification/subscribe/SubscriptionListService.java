package com.quarantined.notification.subscribe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubscriptionListService {

	private final SubscriptionListRepository subscriptionListRepository;
	
	public SubscriptionList createSubscription(String channel) {
		SubscriptionList subscribeChannel = new SubscriptionList();
		subscribeChannel.setChannel(channel);
		return subscriptionListRepository.save(subscribeChannel);
	}
	
	public List<SubscriptionList> findAll(){
	
		return subscriptionListRepository.findAll();
	}
}
