package com.quarantined.notification.subscribe;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionListRepository extends JpaRepository<SubscriptionList, Long> {

}
