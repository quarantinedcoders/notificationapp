package com.quarantined.notification.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@ConfigurationProperties(prefix = "app")
public class AppProperties {
	private final Auth auth = new Auth();
	private final OAuth2 oauth2 = new OAuth2();
	private final GoogleMailService googleMailService = new GoogleMailService();

	@Data
	public static class Auth {
		private String tokenSecret;
		private long tokenExpirationMsec;
	}

	@Data
	public static final class OAuth2 {
		private List<String> authorizedRedirectUris = new ArrayList<>();
	}

	@Data
	public static final class GoogleMailService {

		private String account;
		private String password;
		private String smtpHost;
		private String smtpPort;
		private String smtpAuth;
		private String smtpStarttlsEnable;
	}
}
