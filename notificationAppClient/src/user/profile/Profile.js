import React, { Component } from 'react';
import TopicList from '../../topic/TopicList';
import { savedetails, getUserProfile } from '../../util/APIUtils';
import { Avatar, Tabs } from 'antd';
import { getAvatarColor } from '../../util/Colors';
import { formatDate } from '../../util/Helpers';
import LoadingIndicator  from '../../common/LoadingIndicator';
import './Profile.css';
import NotFound from '../../common/NotFound';
import ServerError from '../../common/ServerError';
import {
    PHONENO_MIN_LENGTH, PHONENO_MAX_LENGTH
} from '../../constants';
import { Form, Input, Button, notification } from 'antd';
const FormItem = Form.Item;

const TabPane = Tabs.TabPane;

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            isLoading: false
        }
        this.loadUserProfile = this.loadUserProfile.bind(this);
    }

    loadUserProfile(email) {
        this.setState({
            isLoading: true
        });

        getUserProfile(email)
        .then(response => {
            this.setState({
                user: response,
                isLoading: false
            });
        }).catch(error => {
            if(error.status === 404) {
                this.setState({
                    notFound: true,
                    isLoading: false
                });
            } else {
                this.setState({
                    serverError: true,
                    isLoading: false
                });
            }
        });
    }

    componentDidMount() {
        const email = this.props.match.params.email;
        this.loadUserProfile(email);
    }

    componentDidUpdate(nextProps) {
        if(this.props.match.params.email !== nextProps.match.params.email) {
            this.loadUserProfile(nextProps.match.params.email);
        }
    }

    render() {
        if(this.state.isLoading) {
            return <LoadingIndicator />;
        }

        if(this.state.notFound) {
            return <NotFound />;
        }

        if(this.state.serverError) {
            return <ServerError />;
        }

        const tabBarStyle = {
            textAlign: 'center'
        };

        return (
            <div className="profile">
                {
                    this.state.user ? (
                        <div className="user-profile">
                            <div className="user-details">
                                <div className="user-avatar">
                                    <Avatar className="user-avatar-circle" style={{ backgroundColor: getAvatarColor(this.state.user.name)}}>
                                        {this.state.user.name[0].toUpperCase()}
                                    </Avatar>
                                </div>
                                <div className="user-summary">
                                        <div className="full-name">{this.state.user.name}</div>
                                    <label><b>User Name:  </b>{this.state.user.username}
                                    </label>
                                    <br></br>
                                    <label><b>Email:  </b>{this.state.user.email}
                                    </label>
                                </div>
                            </div>
                            <div className="user-form">
                                  {'\n'} <SaveDetailsForm {...this.state} />
                            </div>
                        </div>
                ): null
            }
          </div>
        );
      }
    }

    class SaveDetailsForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phoneno: {
                value: props.user.phoneno || ''
            },
            user: props.user
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);
    }

    handleInputChange(event, validationFun) {
        const target = event.target;
        const inputName = target.name;
        const inputValue = target.value;
        this.setState({
            [inputName] : {
                value: inputValue,
                ...validationFun(inputValue)
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        const userrequest = this.state.user;
        userrequest.phoneno = this.state.phoneno.value;


        savedetails(userrequest)
        .then(response => {
            notification.success({
                message: 'Notification App',
                description: "Thank you! You have successfully saved your details",
            });
        }).catch(error => {
            notification.error({
                message: 'Notification App',
                description: error.message || 'Sorry! Something went wrong. Please try again!'
            });
        });
    }

    isFormInvalid() {
        return !(this.state.phoneno.validateStatus === 'success');
    }

    render() {
        return (
            <div className="savedetails-container">
                <div>
                    <Form onSubmit={this.handleSubmit} className="signup-form">
                        <FormItem
                            label="Phone Number"
							              validateStatus={this.state.phoneno.validateStatus}
                            help={this.state.phoneno.errorMsg}>
                            <Input
                                size="large"
                                name="phoneno"
                                autoComplete="off"
                                placeholder="Phone Number with counry code"
                                value={this.state.phoneno.value}
                                onChange={(event) => this.handleInputChange(event, this.validatePhoneno)} />
                        </FormItem>
                        <FormItem>
                            <Button type="primary"
                                htmlType="submit"
                                size="large"
                                className="btn btn-block btn-primary"
                                disabled={this.isFormInvalid()}>Save</Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
        );
    }

    // Validation Functions

	validatePhoneno = (phoneno) => {
        if(phoneno.length < PHONENO_MIN_LENGTH) {
            return {
                validateStatus: 'error',
                errorMsg: `Phoneno is too short (Minimum ${PHONENO_MIN_LENGTH} characters needed.)`
            }
        } else if (phoneno.length > PHONENO_MAX_LENGTH) {
            return {
                validationStatus: 'error',
                errorMsg: `Phoneno is too long (Maximum ${PHONENO_MAX_LENGTH} characters allowed.)`
            }
        }
        const PHONENO_REGEX = RegExp('^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$');
                if(!PHONENO_REGEX.test(phoneno)) {
                    return {
                        validateStatus: 'error',
                        errorMsg: 'Phone Number not valid'
                    }
                }
        else {
            return {
                validateStatus: 'success',
                errorMsg: null,
            };
        }
    }

}

export default Profile;
