import React, { Component } from 'react';
import { getSubscriptionList, createTopic, checkAdminUser, getCurrentUser } from '../util/APIUtils';
import '../topic/NewTopic.css';
import { Form, Input, Button, notification } from 'antd';
import LoadingIndicator from '../common/LoadingIndicator';
import { withRouter, Link } from 'react-router-dom';
const FormItem = Form.Item;
const { TextArea } = Input

class NewTopic extends Component {

    constructor(props) {
        super(props);
        this.state = {

            channelnames: [],
            list: [],
            name: "",
            flag: true,
            isLoading: false
        }

    }

    handleQuestionChange = (event) => {
        const value = event.target.value;
        this.setState({
            name: value,
            ...this.validateTopicName(value)
        }
        );
    }

    validateTopicName = (nameText) => {
        if (nameText.length === 0) {
            return {
                validateStatus: 'error',
                errorMsg: 'Please enter your name!'
            }
        }
        else {

            return {
                validateStatus: 'success',
                errorMsg: null
            }
        }
    }

    getSubscriptions = () => {
        getSubscriptionList()
        .then(res=>{
            this.setState((prevState) => ({
                isLoading : !prevState.isLoading
            ,
               list : res}
            ))
        })
    }

    checkUserRole = () => {
        checkAdminUser(this.props.currentUser.username)
            .then(res => {
                this.setState({ flag: res },
                )

            })
    }

    returnType = (event) => {

        const name = event.target.name
        const value = event.target.checked

        if (value) {
            const updatedlist = this.state.channelnames.concat(name)

            this.setState({
                channelnames: updatedlist
            })
        }
        else {
            const updatedlist = this.state.channelnames
            delete updatedlist[updatedlist.indexOf(name)]
            this.setState({
                channelnames: updatedlist
            })

            const filtered = this.state.channelnames.filter(function (el) {
                return el != null;
            });

            this.setState({
                channelnames: filtered
            })
        }
    }

    componentDidMount() {
        this.getSubscriptions()
        this.props.currentUser ? this.checkUserRole() : null
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const topicData = {
            name: this.state.name,
            channels: this.state.channelnames.map(channel => {
                return { name: channel }
            }),
            topicLength: {
                days: 7,
                hours: 0
            }
        }

        createTopic(topicData)
            .then(response => {
                this.props.history.push("/");
            }).catch(error => {
                if (error.status === 401) {
                    this.props.handleLogout('/login', 'error', 'You have been logged out. Please login create topic.');
                } else {
                    notification.error({
                        message: 'Notification App',
                        description: error.message || 'Sorry! Something went wrong. Please try again!'
                    });
                }
            });

    }

    isFormInvalid = () => {
        if (this.state.channelnames.length && this.state.channelnames) {
            return false
        }
        else {
            return true
        }
    }

    render() {
        const { list } = this.state;
        let FormItems = [this.state.isLoading ? 
            <div className="new-topic-container">
                <h1 className="page-title">Create Topic</h1>
                <div className="new-topic-content">
                    <Form onSubmit={this.handleSubmit} className="create-topic-form">
                        <FormItem validateStatus={this.state.name.validateStatus}
                            help={this.state.name.errorMsg} className="topic-form-row">
                            <TextArea
                                placeholder="Enter App name"
                                style={{ fontSize: '16px' }}
                                autosize={{ minRows: 3, maxRows: 6 }}
                                name="name"
                                value={this.state.name}
                                onChange={this.handleQuestionChange} required />
                        </FormItem>

                        <label><h2>Select Channels Available For User Subscription</h2></label>

                        {
                            list.map((listitem) => (
                                <FormItem className="topic-form-row-checkbox">
                                    <label>
                                        <input type="checkbox"
                                            value={listitem.channel}
                                            name={listitem.channel}
                                            key={listitem.id}
                                            onChange={this.returnType} />
                                        {listitem.channel}
                                    </label>
                                </FormItem>
                            ))
                        }

                        <FormItem className="topic-form-row">
                            <Button type="primary"
                                htmlType="submit"
                                size="large"
                                disabled={this.isFormInvalid()}
                                className="create-topic-form-button">Create Topic</Button>
                        </FormItem>
                    </Form>
                </div>
            </div>
            : <LoadingIndicator />]

        let loginArray = [
           <div className="loginArray"> <h2>You Must Login To Create New Topic</h2>
            <p>Click Here To  <Link to="/login">Login</Link></p>
            </div>
        ]

        if(this.props.currentUser && !this.state.flag){
                this.props.history.push("/")
                notification.info({
                    message: 'Notification App',
                    description: "Only Admin Can Create New Topic",
                });
            }
        
        return(
            this.props.currentUser ? FormItems : loginArray
        )
        }               
}

export default NewTopic;