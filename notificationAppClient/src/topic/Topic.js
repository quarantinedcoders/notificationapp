import React, { Component } from 'react';
import './Topic.css';
import { Icon } from 'antd';

import { Button, Checkbox } from 'antd';

class Topic extends Component {

    isSelected = (channel) => {
      if(this.props.topic.selectedChannel) {
        return this.props.topic.selectedChannel.indexOf(channel.id)>-1;
      } else {
        return false;
      }
    }

    render() {
        const topicChannels = [];
            this.props.topic.channels.forEach(channel => {
                topicChannels.push(<Checkbox key={channel.id} value={channel.id} >{channel.name}</Checkbox>);
            })
        return (
            <div className="topic-content">
                <div className="topic-header">

                    <div className="topic-name">
                        {this.props.topic.name}
                    </div>
                </div>
                <div className="topic-channels">
                    <Checkbox.Group
                        className="topic-channel-radio-group"
                        onChange={this.props.handleSubscribeChange}
                        value={this.props.currentSubscribe}>
                       { topicChannels }
                    </Checkbox.Group>
                </div>
                <div className="topic-footer">
                    <Button className="subscribe-button" disabled={this.props.currentSubscribe == undefined ? true : false} onClick={this.props.handleSubscribeSubmit}>Subscribe</Button>
                </div>
            </div>
        );
    }
}

export default Topic;
