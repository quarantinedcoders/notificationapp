export const SERVER_BASE_URL = process.env.REACT_APP_API_BASE_URL ? process.env.REACT_APP_API_BASE_URL + ':8080' : 'http://localhost:8080';
export const CLIENT_BASE_URL = process.env.REACT_APP_API_BASE_URL ? process.env.REACT_APP_API_BASE_URL + ':9090' : 'http://localhost:3000';

export const API_BASE_URL = SERVER_BASE_URL + '/api'

export const OAUTH2_REDIRECT_URI = CLIENT_BASE_URL + '/oauth2/redirect';
export const GOOGLE_AUTH_URL = SERVER_BASE_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;

export const ACCESS_TOKEN = 'accessToken';

export const TOPIC_LIST_SIZE = 30;
export const MAX_CHOICES = 6;
export const TOPIC_NAME_MAX_LENGTH = 140;
export const TOPIC_CHANNEL_MAX_LENGTH = 40;

export const NAME_MIN_LENGTH = 4;
export const NAME_MAX_LENGTH = 40;

export const USERNAME_MIN_LENGTH = 3;
export const USERNAME_MAX_LENGTH = 15;

export const EMAIL_MAX_LENGTH = 40;

export const PHONENO_MIN_LENGTH = 13;
export const PHONENO_MAX_LENGTH = 15;

export const PASSWORD_MIN_LENGTH = 6;
export const PASSWORD_MAX_LENGTH = 20;

export const USERNAME_REGEX = /^[a-zA-Z0-9]{3,15}$/i;
export const PASSWORD_REGEX = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])[\w\d!@#$%_-]{6,20}$/i;

export const EMAIL_REGEX = /((^[a-zA-Z]+(?:[a-zA-Z0-9])*)+((?:[._-][0-9a-zA-Z]+)*)+(?:[.-_][0-9a-zA-Z]+)*)@([0-9a-zA-Z]+(?:[._-][0-9a-zA-Z]+)*\.[0-9a-zA-Z]{2,3})$/i;
export const FULLNAME_REGEX = /^([a-zA-Z]+(?:[\s]*)+(?:[a-zA-Z])+(?:[\s]*)+(?:[a-zA-Z])+(?:[\s]*)+(?:[a-zA-Z]{1,}))$/i;




